
import os
import platform
import subprocess

def open_file(path):
    if platform.system() == "Windows":
        os.startfile(path)
    elif platform.system() == "Darwin":
        subprocess.Popen(["open", path])
    else:
        subprocess.Popen(["xdg-open", path])

seperator = '<span style="display: inline; font-family: Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif;"> | </span>'


def generate_signature(name, job_title, email, direct_dial, mobile, linkedin):
    linkedin_block = '<a style="text-decoration: none; display: inline;" href="LINKEDINURL"><img width="24" style="margin-bottom:2px; border:none; display:inline;" height="24" data-filename="linkedin.png" src="https://s3.amazonaws.com/htmlsig-assets/polygon/linkedin.png" alt="LinkedIn"></a>'
    
    template = open('input/template.html', 'r', encoding='utf-8')
    code = template.read()
    code = code.replace('{{name}}', name)
    
    if job_title:
        code = code.replace('{{job_title}}', job_title)
        code = code.replace('{{name_title_seperator}}', seperator)
    elif not job_title:
        code = code.replace('{{job_title}}', '')
        code = code.replace('{{name_title_seperator}}', '')
    
    if direct_dial:
        code = code.replace('{{direct_dial}}', f'DD: {direct_dial}')
        if email:
            code = code.replace('{{direct_dial_seperator}}', seperator)
        if not email:
            code = code.replace('{{direct_dial_seperator}}', '')
            
    elif not direct_dial:
        code = code.replace('{{direct_dial}}', '')
        code = code.replace('{{direct_dial_seperator}}', '')

    if mobile:
        code = code.replace('{{mobile}}', f'M: {mobile}')
    elif not mobile:
        code = code.replace('{{mobile}}', '')

    if linkedin:
        linkedin_block = linkedin_block.replace('LINKEDINURL', linkedin)
        code = code.replace('{{linkedin}}', linkedin_block)
    elif not linkedin:
        code = code.replace('{{linkedin}}', '')

    code = code.replace('{{email}}', email)
    filename = f"{''.join(name.split())}-signature.html"
    path = f"output/{filename}"
    
    with open(path, "w+") as f:
        f.write(code)

    print(f'Generated {path}')
    # open_file(path)
    return filename
