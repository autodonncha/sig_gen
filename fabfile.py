from invoke import Exit
# from invocations.console import confirm
from fabric import task

my_hosts = ["root@46.101.13.247"]

# @task
# def commit(c):
#     c.local("git add -p && git commit")

# @task
# def push(c):
#     c.local("git push")

# @task
# def prepare_deploy(c):
#     test(c)
#     commit(c)
    # push(c)

@task(hosts=my_hosts)
def deploy(c):
    code_dir = "./signature_generator"
    # if not c.run("test -d {}".format(code_dir), warn=True):
    #     cmd = "git clone user@vcshost:/path/to/repo/.git {}"
    #     c.run(cmd.format(code_dir))
    c.local("git add .")
    c.local(f'git commit -m "ok"')
    c.local('git push production master')
    c.run('supervisorctl restart signature_generator')

