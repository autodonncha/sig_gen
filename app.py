from flask import Flask, jsonify, request, render_template, redirect, url_for, flash, send_file, send_from_directory
from generate_signature import generate_signature
app = Flask(__name__)
OUTPUT_FOLDER = f'{app.root_path}/output'

app.config['OUTPUT_FOLDER'] = OUTPUT_FOLDER


# @app.route('/')
# def index():
#     return 'Flask is running!'








	# </div>
	# <div class="form-group">
	# 	<label for="job_title">Job Title</label>
	# 	<input class="form-control" name="job_title" id="job_title" required>
	# </div>
	# <div class="form-group">
	# 	<label for="email">Email</label>
	# 	<input class="form-control" name="email" id="email" required>
	# </div>
	# <div class="form-group">
	# 	<label for="direct_dial">Direct Dial</label>
	# 	<input class="form-control" name="direct_dial" id="direct_dial" required>
	# </div>
	# <div class="form-group">
	# 	<label for="mobile">Mobile</label>
	# 	<input class="form-control" name="mobile" id="mobile" required>
	# </div>
	# <div class="form-group">
	# 	<label for="linkedin">LinkedIn</label>
	# 	<input class="form-control" name="linkedin" id="linkedin" required>
	# </div>





@app.route('/', methods=('GET', 'POST'))
def create():
    print(app.config['OUTPUT_FOLDER'])
    if request.method == 'POST':
        name = request.form['name']
        job_title = request.form['job_title']
        email = request.form['email']
        direct_dial = request.form['direct_dial']
        mobile = request.form['mobile']
        linkedin = request.form['linkedin']
        # db = get_db()
        error = None
        # elif db.execute(
        #     'SELECT id FROM user WHERE username = ?', (username,)
        # ).fetchone() is not None:
        #     error = 'User {} is already registered.'.format(username)
        filename = generate_signature(name, job_title, email, direct_dial, mobile, linkedin)
        print(filename)
        if error is None:
            # db.execute(
            #     'INSERT INTO user (username, password) VALUES (?, ?)',
            #     (username, generate_password_hash(password))
            # )
            # db.commit()
            return send_from_directory(app.config['OUTPUT_FOLDER'], filename, as_attachment=True)

        flash(job_title)

    return render_template('create.html')

@app.route('/success')
def success():
    return render_template('success.html')


if __name__ == '__main__':
    app.run()
